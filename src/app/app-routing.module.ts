import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ClientsComponent } from './clients/clients.component';
import { BillingComponent } from './billing/billing.component';

const routes: Routes = [
  { path: '', component:  HomeComponent},
  { path: 'clientes', component: ClientsComponent },
  { path: 'facturacion', component: BillingComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
