import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { FormControl } from '@angular/forms';

import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {MomentDateAdapter, MatMomentDateAdapterOptions} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// import * as Moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
// import {default as _rollupMoment} from 'moment';

// const moment = _rollupMoment || _moment;
const moment = _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


export interface Products {
  value: string;
  product: string;
}

export interface Sellers {
  value: string;
  name: string;
}

export interface Clients {
  value: string;
  name: string;
  nit: string;
}

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})


export class BillingComponent implements OnInit {
  // billingDate = new FormControl(moment());
  // paymentDate = new FormControl(moment());

  // myControl = new FormControl('');
  email = new FormControl('', [Validators.required, Validators.email]);
  authorizationNumber = new FormControl('', [Validators.required, Validators.maxLength(4)]);
  selectedProd = new FormControl('',  [Validators.required]);
  cantidad = new FormControl('',  [Validators.required]);
  salesDate = new FormControl(moment(), Validators.required);
  billingDate = new FormControl(moment(), Validators.required);
  paymentDate = new FormControl(moment(), Validators.required);

  products: Products[] = [
    {value: '1', product: 'Ibuprofeno'},
    {value: '2', product: 'Aspirina'},
    {value: '3', product: 'Noxpirin'}
  ];
  sellers: Sellers[] = [
    {value: '1', name: 'Benito Perez'},
    {value: '2', name: 'Ana Lopez'},
    {value: '3', name: 'Marco Baptista'},
  ];
  clients: Clients[] = [
    {value: '1', name: 'Farmacorp', nit: '2867162013'},
    {value: '2', name: 'FarmaBolivia', nit: '357257020'},
    {value: '3', name: 'FarmaElias', nit: '5242285018'},
  ];

  producto: string;
  // cantidad: number;
  precio: number;
  subtotal: number;
  descuento: number;
  total: number;
  // selectedProd: string;
  selectedProdName: Products;
  billingNumber: number;
  // authorizationNumber1: number;
  sellerName: string;
  selectedClient: string;
  clientId: string;
  clientName: Clients;
  noteNumber: number;
  payAmounts: string;

  // public ownerForm: FormGroup;

  // profileForm = new FormGroup({
  //   billingNum :  new FormControl()
  // });

  constructor(private fb: FormBuilder) {
    this.subtotal = 0;
    this.total = 0;
    this.precio = 0;
    this.billingNumber = 100000;
    // this.authorizationNumber1 = 332401800004979;
    this.noteNumber = 1000;
    // this.selectedProdName = new Products('','');
  }

  // getErrorMessage() {
  //   return this.email.hasError('required') ? 'You must enter a value' :
  //       this.email.hasError('email') ? 'Not a valid email' :
  //           '';
  // }

  getErrorNumAuth() {
    return this.authorizationNumber.hasError('required') ? 'Numero de Autorizacion requerido' : '';
  }

  getErrorProd() {
    // console.log('getErrorProd [this.selectedProd]', this.selectedProd.getError);
    return this.selectedProd.hasError('required') ? 'Debes ingresar un producto' : '';
  }

  getErrorCant() {
    return this.cantidad.hasError('required') ? 'Debes ingresar una cantidad' :
        this.cantidad.hasError('email') ? 'Not a valid email' :
            '';
  }
  getErrorSalesDate() {
    return this.salesDate.hasError('required') ? 'Debes ingresar una fecha valida' : '';
  }

  setPrice(idProd: string) {
    const iPr = Number(idProd);
    if (!isNaN(iPr)) {
      if (this.selectedProd.value === '1') {
        this.precio = 10;
      } else if (this.selectedProd.value === '2') {
        this.precio = 100;
      } else if (this.selectedProd.value === '3') {
        this.precio = 1000;
      } else {
        this.precio = -1;
      }
    }
  }
  calculate( cant: number) {
    // this.cantidad = cant;
    // this.setPrice(this.cantidad);
    console.log('calculate [this.cantidad]:', this.cantidad.value);
    if (this.cantidad.value) {
      this.subtotal = this.cantidad.value * this.precio;
      this.total = (this.cantidad.value * this.precio);
      console.log('calculate [this.total]: ', this.total);
    }
  }

  calculateDesc(desc: number) {
    this.descuento = desc;
    if (!isNaN(desc)) {
      this.total = this.total - this.descuento;
      console.log('calculateDesc: ', this.total);
    }
  }

  changeProd(idProd: string) {
    console.log('changeProd [idProd]:', idProd);
    console.log('changeProd [this.selectedProd.value]:', this.selectedProd.value);
    // this.selectedProd = idProd;
    // console.log('changeProd [this.cantidad]:', this.cantidad);
    // console.log('changeProd [this.selectedProd]:', this.selectedProd);
    this.setPrice(this.selectedProd.value);
    if (this.cantidad.value) {
      this.calculateDesc(this.descuento);
      this.calculate(this.cantidad.value);
    }
    // console.log('changeProd [this.selectedProd]:', this.selectedProd);
    this.selectedProdName = this.products.filter(x => x.value === this.selectedProd.value)[0];
    console.log('changeProd [this.selectedProdName]:', this.selectedProdName);
    // console.log('salesDate: ', this.salesDate.value.format('DD/MM/YYYY h:mm:ss a'));
    // console.log('billingDate: ', this.billingDate.value.format('DD/MM/YYYY h:mm:ss a'));
    // console.log('paymentDate: ', this.paymentDate.value.format('DD/MM/YYYY h:mm:ss a'));
  }

  changeSeller(val) {
    console.log(val);
  }

  changeClient(id) {
    this.clientId = id;
    this.clientName = this.clients.filter(x => x.value === this.clientId)[0];
    console.log(this.clientId);
    console.log(this.clientName.nit);

  }

  changeNoteNum(val) {
    console.log(val);
  }

  changePayAmount(val) {
    console.log(val);
  }

  // addEventPS(type: string, event) {
    // this.events.push(`${type}: ${event.value}`);
    // this.salesDate = toDateString(event);
    // let date = event.toDate();
    // console.log(event);
  //   console.log(date.toLocaleTimeString(undefined, {
  //     day: '2-digit',
  //     month: '2-digit',
  //     year: 'numeric'
  // }));
    // .value);
  // }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.selectedProd.value);
  }

  ngOnInit() {
  }

}
