import { NgModule } from '@angular/core';
// import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatMenuModule,
  MatIconModule,
  MatToolbarModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatListModule,
  MatDividerModule,
  MatDatepickerModule,
  MatDatepicker,
  MatNativeDateModule,
  // MatDatepickerInputEvent,
  // MatDatepickerInput,
  // MatDatepickerInputEvent,
} from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatListModule,
    MatDividerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    // MatDatepickerInputEvent,
    // MatDatepicker,
    // MatDatepickerInput,
    // MatDatepickerInputEvent,
  ],
  exports: [
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatListModule,
    MatDividerModule,
    MatDatepickerModule,
    MatDatepicker,
    MatNativeDateModule,
    // MatDatepickerInputEvent,
    // MatDatepickerInput,
    // MatDatepickerInputEvent,
  ],
  // providers: [ MatDatepickerModule ],
})

export class MaterialModule { }